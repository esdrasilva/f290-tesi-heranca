package f290.tesi.heranca;

import f290.tesi.heranca.model.Pessoa;
import f290.tesi.heranca.model.PessoaFisica;

public class F290TesiHeranca {

    public static void main(String[] args) {
        // TODO code application logic here
        
        Pessoa pessoa = new Pessoa("Eduardo Rocha", "eduardo@gmail.com", "99999-8855");
        PessoaFisica pessoaFisica = new PessoaFisica("Victoria W", "victoria@gmail.com", "75555-4477","7891000100103");

        System.out.println(pessoa);
        System.out.println(pessoaFisica);
                
    }
    
}
