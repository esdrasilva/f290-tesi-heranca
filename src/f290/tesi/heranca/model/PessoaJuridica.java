/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package f290.tesi.heranca.model;

/**
 *
 * @author esdras
 */
public class PessoaJuridica extends Pessoa {
    
    private String cnpj;
    private String razaoSocial;

    public PessoaJuridica(String nome, String email, String telefone, String cnpj) {
        super(nome, email, telefone);
        this.cnpj = cnpj;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
    
    
    
        @Override
    public String toString() {
        return String.format("PessoaJuridica{nome: %s - email: %s - telefone: %s - endereco: %s - CNPJ: %s}", this.getNome(), this.getEmail(), this.getTelefone(), this.getEndereco(), this.getCnpj());
    }
        
}
