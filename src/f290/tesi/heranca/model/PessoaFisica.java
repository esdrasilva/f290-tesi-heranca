package f290.tesi.heranca.model;

public class PessoaFisica extends Pessoa {
    
    private String cpf;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }    

    public PessoaFisica(String nomePF, String emailPF, String telefonePF, String cpfPF){
        super(nomePF, emailPF, telefonePF);
        this.cpf = cpfPF;
    }

    @Override
    public String toString() {
        return String.format("PessoaFisica{nome: %s - email: %s - telefone: %s - endereco: %s - CPF: %s}", this.getNome(), this.getEmail(), this.getTelefone(), this.getEndereco(), this.getCpf());
    }
    
    
}
