package f290.tesi.heranca.model;

public class Pessoa {
    // Atributos
    private String nome;
    private String email;
    private String telefone;
    private String endereco = "Não fornecido";
    
    // Comportamentos
    public Pessoa(String nome, String email, String telefone) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (email.contains("@")) {
            this.email = email;
        } else {
            System.out.println("Digite um e-mail válido, inclua @...");
        }
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }        

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"{" + "nome=" + nome + ", email=" + email + ", telefone=" + telefone + ", endereco="+endereco+'}';
    }    
    
}
